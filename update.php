<?php
session_start();

/*
Haft-e-Khabis

By Samic.
(samic.org)

Created on March 20, 2020 
Updated on March 23, 2020 

*/

include "database.php";

if (! isset($_SESSION["user"]))  ShowText_Exit("You cannot access this page directly.");

$user = $_SESSION["user"];
$game_id = $_SESSION["game_id"];


function CardHTML($card){
    $rank = substr($card, 0, 1);
    $suit = substr($card, 1, 2);
    if ($rank == "0")   $rank = "10";
    $rank_l = strtolower($rank);
    switch ($suit) {
        case "D":
            $suit = "diams";
            break;
        case "C":
            $suit = "clubs";
            break;
        case "H":
            $suit = "hearts";
            break;
        case "S":
            $suit = "spades";
            break;
    }

$str = <<<EOD
<div class="card rank-$rank_l $suit">
<span class="rank">$rank</span><span class="suit">&$suit;</span>
</div>
EOD;

    return $str;
}


// Get the latest log
$sql = "SELECT `gamelog` FROM `log` WHERE game='{$game_id}';";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
$rows = mysqli_fetch_array($result);
$log = substr($rows['gamelog'], -2000);


// Get the users list and zamin
$user_list = '
        <table border="1" style="border: 1px solid black;border-collapse: collapse;">
        <tr>
        <td style="padding: 10px;"><b>Name</b></td>
        <td style="padding: 10px;"><b>Cards</b></td>
        </tr>';
$sql = "SELECT * FROM `game` WHERE game='{$game_id}' ORDER BY ID;";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
while ($rows = mysqli_fetch_array($result)) {
    if ($rows['user'] != "zamin"){
        $user_card_num = strlen($rows['hand']) / 2;
        if (!isset($admin))  $admin = $rows['user'];
        if (isset($admin) && ($admin == $user) && ($rows['user'] != $user)){
            $user_list .= <<<EOD
<tr>
    <td style='padding: 10px;'>
        <span style='float: left;'>{$rows['user']}</span>
         &nbsp;&nbsp;
        <span style='float: right;'>
        <a href='boot.php?game_id={$game_id}&user_boot={$rows['user']}' target='_blank' title='Boot this player out of the game!'>
            <span style='font-size: 0.5em;'>X</span>
        </a>
        </span>
    </td>
    <td style='padding: 10px;'>{$user_card_num}</td>
</tr>
EOD;

        }else{ 

            $user_list .= <<<EOD
<tr>
    <td style='padding: 10px;'>{$rows['user']}</td>
    <td style='padding: 10px;'>{$user_card_num}</td>
</tr>
EOD;

        }

    }else {

        $zamin = $rows['hand'];
    }
}
$user_list .= "</table>";


header("Content-type: application/json");
echo json_encode(array(CardHTML($zamin), $log, $user_list));

?>
