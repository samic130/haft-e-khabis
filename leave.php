<?php
session_start();

/*
Haft-e-Khabis

By Samic.
(samic.org)

Created on March 20, 2020 
Updated on March 23, 2020 

*/

include "database.php";


$user = $_SESSION["user"];
$game_id = $_SESSION["game_id"];


// Get the latest log
$sql = "SELECT `gamelog` FROM `log` WHERE game='{$game_id}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
$rows = mysqli_fetch_array($result);
$log = $rows['gamelog'];


$news = $user . " left the game at " . date("h:i:s");
$log .= "\n" . $news;
$sql = "UPDATE `log` SET gamelog='{$log}' WHERE game='{$game_id}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));


$sql = "DELETE FROM `game` WHERE user='{$user}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));


session_unset();
session_destroy();
session_write_close();
setcookie(session_name(),'',0,'/');


ShowText_Exit("Thanks for playing!<br>See you later ...");


?>
