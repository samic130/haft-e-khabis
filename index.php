<?php
session_start();

/*
Haft-e-Khabis

By Samic.
(samic.org)

Created on March 20, 2020 
Updated on April 3, 2020 

*/

include "database.php";

$main_page ='
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<title>Haft-e-Khabis</title>
</head>
<frameset rows="90%,10%" border="0">
    <frame src="player.php">
    <frame src="chat.php">
</frameset>
</html>';

if (isset($_SESSION["user"]) && isset($_SESSION["game_id"]))  exit($main_page);     // automatically re-login a user that left the page

function card_gen($num){
    $card_nums = array("A", "2", "3", "4", "5", "6", "7", "8", "9", "0", "J", "Q", "K");    // 0 is 10
    $card_suits = array("D", "C", "H", "S");    // diamonds (♦), clubs (♣), hearts (♥) and spades (♠)
    $cards = '';
    for ($i = 0; $i < $num; $i++){
        $cards = $cards . $card_nums[rand(0, 12)] . $card_suits[rand(0, 3)];
    }
return $cards;
}

if (isset($_REQUEST['gameid']))  $_REQUEST['game_id'] = $_REQUEST['gameid'];
if (isset($_REQUEST['id']))  $_REQUEST['game_id'] = $_REQUEST['id'];
if (isset($_REQUEST['game']))  $_REQUEST['game_id'] = $_REQUEST['game'];


if (isset($_REQUEST['game_id'])){

    $_REQUEST['game_id'] = mysqli_real_escape_string($DBlink, $_REQUEST['game_id']);

    $sql = "SELECT `user` FROM `game` WHERE game='{$_REQUEST['game_id']}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    if (mysqli_num_rows($result) < 1)  ShowText_Exit("Invalid Game ID<br>Click on the invitation link again.");

    $_SESSION["game_id"] = $_REQUEST['game_id'];

}else{
    ShowText_Exit("You need to use an invitation link.<br><br>Click on the invitation link again<br><br>Or<form action='admin.php'><input type='submit' value='start a new group'></form>");
}

if (isset($_REQUEST['user'])){

    $_REQUEST['user'] = mysqli_real_escape_string($DBlink, ucfirst($_REQUEST['user']));

    if ((ctype_alnum($_REQUEST['user']) == false) || (strlen($_REQUEST['user']) < 1) || (strlen($_REQUEST['user']) > 49)){
        ShowText_Exit("That's not an acceptable name. Choose something else.");
    }

    $sql = "SELECT `hand` FROM `game` WHERE game='{$_REQUEST['game_id']}' AND user='{$_REQUEST['user']}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    if (mysqli_num_rows($result) < 1) {

        $sql = "INSERT INTO `game` (game, user, hand) VALUES ('{$_REQUEST['game_id']}', '{$_REQUEST['user']}', '" . card_gen(7) . "')";
        $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));

    }else{
        ShowText_Exit("A player is playig with this name right now!<br><br>Choose another name.<br><br>");
    }

    // Get the latest log
    $sql = "SELECT `gamelog` FROM `log` WHERE game='{$_REQUEST['game_id']}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    $rows = mysqli_fetch_array($result);
    $log = $rows['gamelog'];

    $news = $_REQUEST['user'] . " joined the game at " . date("h:i:s");
    $log .= "\n" . $news;
    $sql = "UPDATE `log` SET gamelog='{$log}' WHERE game='{$_REQUEST['game_id']}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));


    $_SESSION["user"] = $_REQUEST['user']; // you're logged in!
    echo $main_page;


}else{


?><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<title>Haft-e-Khabis</title>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<link rel='stylesheet' type='text/css' href='phpmyadmin.css'>
<style type='text/css' media='screen'>
* {
	margin:0;
	padding:0;
}
div#banner {
	position:absolute;
	top:0;
	right:0;
	margin:0;
	background-color:#eee;
	width:150px;
	border-radius:8px;
}
div#banner-content {
	width:150px;
	margin:0 auto;
	padding:10px;
	border:0px;
}
div#main-content {
	padding: 70px;
}
</style>
</head>

<body class='loginform'>
<div class='container'>
	<p>
		<br>
	</p>
	<form method='post' name='login_form' class='login'>
		<fieldset>
			<legend>Join the game</legend>
			<div class='item'>
				<label for='input_username'>Type your name:</label>
				<input type='text' id='user' name='user' size='15' class='textfield'>
                <input type='hidden' name='game_id' value='<?php echo $_REQUEST['game_id']; ?>'>
			</div>
		</fieldset>
		<fieldset class='tblFooters'>
			<input value=' Join ' type='submit' id='input_go'>
		</fieldset>
	</form>
</div>
<p>
</p>
<a style='text-decoration: none;' target='_blank' href='https://samic.org'><i><code style='color: rgb(153, 153, 153); font-size:11px'>By Samic.</code></i></a>
<br>
<a style='text-decoration: none;' target='_blank' href='https://gitlab.com/samic130/haft-e-khabis'><i><code style='color: rgb(153, 153, 153); font-size:11px'>(See the source code here)</code></i></a>
<script>document.getElementById('user').focus();</script>
</body>
</html>

<?php 
}
?>
