<?php
session_start();

/*
Haft-e-Khabis

By Samic.
(samic.org)

Created on March 20, 2020
Updated on March 30, 2020

*/

include "database.php";
date_default_timezone_set('America/Chicago');

if (! isset($_SESSION["user"]))  ShowText_Exit("You need to sign in.<br><br>Click <a href=index.php>here</a>.");

$user = $_SESSION["user"];
$game_id = $_SESSION["game_id"];


$card_nums = array("A", "2", "3", "4", "5", "6", "7", "8", "9", "0", "J", "Q", "K");    // 0 is 10
$card_suits = array("D", "C", "H", "S");    // diamonds (♦), clubs (♣), hearts (♥) and spades (♠)


// Get the player's hand
$sql = "SELECT `hand` FROM `game` WHERE game='{$game_id}' AND user='{$user}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
$rows = mysqli_fetch_array($result);
$hand = $rows['hand'];


// Get the latest log
$sql = "SELECT `gamelog` FROM `log` WHERE game='{$game_id}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
$rows = mysqli_fetch_array($result);
$log = $rows['gamelog'];


function UpdateLog($DBlink, $game_id, $log, $news){
    $log .= "\n" . $news;
    $sql = "UPDATE `log` SET gamelog='{$log}' WHERE game='{$game_id}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    return $log;
}


function UpdateHand($DBlink, $game_id, $user, $hand){
    $sql = "UPDATE `game` SET hand='{$hand}' WHERE game='{$game_id}' AND user='{$user}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    if (mysqli_affected_rows($DBlink) != 1) {
        session_unset();
        session_destroy();
        session_write_close();
        ShowText_Exit("You were booted!");
    }
}


function CardTranslate($card){
// diamonds (♦), clubs (♣), hearts (♥) and spades (♠)
    $rank = substr($card, 0, 1);
    $suit = substr($card, 1, 2);
    if ($rank == "0")   $rank = "10";
    switch ($suit) {
        case "D":
            $suit = "Khesht";
            break;
        case "C":
            $suit = "Geshniz";
            break;
        case "H":
            $suit = "Del";
            break;
        case "S":
            $suit = "Pik";
            break;
    }
    return $rank . " of " . $suit;
}


function CardHTML($card, $zamin){
    $rank = substr($card, 0, 1);
    $suit = substr($card, 1, 2);
    if ($rank == "0")   $rank = "10";
    $rank_l = strtolower($rank);
    switch ($suit) {
        case "D":
            $suit = "diams";
            break;
        case "C":
            $suit = "clubs";
            break;
        case "H":
            $suit = "hearts";
            break;
        case "S":
            $suit = "spades";
            break;
    }
$str = <<<EOD
<li>
<a class="card rank-$rank_l $suit" href="player.php?action=drop&card=$card">
<span class="rank">$rank</span><span class="suit">&$suit;</span>
</a>
</li>
EOD;

    if ($zamin == "1"){
$str = <<<EOD
<div class="card rank-$rank_l $suit">
<span class="rank">$rank</span><span class="suit">&$suit;</span>
</div>
EOD;
    }
    return $str;
}

// =========================== END of Functions ======================================


$action = isset($_GET['action']) ? $_GET['action'] : '0';

if ($action == "take"){

    $new_card = $card_nums[rand(0, 12)] . $card_suits[rand(0, 3)];
    $hand .= $new_card;
    UpdateHand($DBlink, $game_id, $user, $hand);

    $news = date("* h:i:s  ") . $user . " took a card";
    $log = UpdateLog($DBlink, $game_id, $log, $news);

}
elseif ($action == "drop"){

    $card = isset($_GET['card']) ? $_GET['card'] : '!';

    // we need to use !== here because (0 != false) evaluates to false!!!  https://www.php.net/manual/en/function.strpos.php
    if ((strpos($hand, $card) !== false) && (strlen($card) == 2) && ctype_alnum($card)) {

        if ($hand == $card)  $user_won = 1;  // this has to be here; before the card is removed from the hand on the next line

        //$hand = str_replace($card, "", $hand);  doesn't work since if you have more than one card of the same type, it removes all of them
        $pos = strpos($hand, $card);
        $hand = substr_replace($hand, "", $pos, 2);
        UpdateHand($DBlink, $game_id, $user, $hand);

        $news = date("* h:i:s  ") . $user . " played " . CardTranslate($card);
        $log = UpdateLog($DBlink, $game_id, $log, $news);

        if (isset($user_won)) {
            $news = "*** {$user} won!!! ***";
            $log = UpdateLog($DBlink, $game_id, $log, $news);
        }

        $sql = "UPDATE `game` SET hand='{$card}' WHERE game='{$game_id}' AND user='zamin'";
        $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));

    }else{
        echo "<h1>Don't try to cheat! </h1><br>";
    }

}


// Get the Zamin
$sql = "SELECT `hand` FROM `game` WHERE game='{$game_id}' AND user='zamin'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
$rows = mysqli_fetch_array($result);
$zamin = $rows['hand'];


// ================================== END OF MAIN CODE ==================================

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <!-- meta http-equiv="refresh" content="10;url=player.php" -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <title>Haft e Khabis</title>
    <link rel="stylesheet" type="text/css" href="cards.css" media="screen" />
    <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="cards-ie.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
        <link rel="stylesheet" type="text/css" href="cards-ie9.css" media="screen" />
    <![endif]-->
    <!-- the following js and css is not part of the CSS cards, but only for this example page -->

</head>
<body>
<center>
<div class="playingCards faceImages rotateHand">
<table>
<tr>
<td width="40%" style="vertical-align:top">

    <h3>Playing Users:</h3>
        <div id="userlist">
        <table border="1" style="border: 1px solid black;border-collapse: collapse;">
        <tr>
        <td style="padding: 10px;"><b>Name</b></td>
        <td style="padding: 10px;"><b>Cards</b></td>
        </tr>
            <?php
            $sql = "SELECT * FROM `game` WHERE game='{$game_id}' ORDER BY ID";
            $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
            while ($rows = mysqli_fetch_array($result)) {
                if ($rows['user'] != "zamin"){
                    $user_card_num = strlen($rows['hand']) / 2;
                    if ($user_card_num == 0)  $game_end = 1;

                    if (!isset($admin))  $admin = $rows['user'];    // the goal here is to define the first user as admin and not anyone else
                    if (isset($admin) && ($admin == $user) && ($rows['user'] != $user)){
                        echo <<<EOD
<tr>
    <td style='padding: 10px;'>
        <span style='float: left;'>{$rows['user']}</span>
         &nbsp;&nbsp;
        <span style='float: right;'>
        <a href='boot.php?game_id={$game_id}&user_boot={$rows['user']}' target='_blank' title='Boot this player out of the game!'>
            <span style='font-size: 0.5em;'>X</span>
        </a>
        </span>
    </td>
    <td style='padding: 10px;'>{$user_card_num}</td>
</tr>
EOD;

                    }else{

                        echo <<<EOD
<tr>
    <td style='padding: 10px;'>{$rows['user']}</td>
    <td style='padding: 10px;'>{$user_card_num}</td>
</tr>
EOD;

                    }

                }
            }
            ?>

        </table>
        </div>
		<br>
            <small><a href="leave.php">Leave the game</a><br></small>
            <br>
            <small><a href="player.php" title="Use this to refresh the page if it stopped updating">Refresh</a><br></small>


<?php
            if (isset($game_end))  echo "<br><a href='admin.php?game_id={$game_id}&newdeal=1' target='_blank'>START A NEW ROUND!</a>";
?>

</td>


<td width="30%" style="vertical-align:top">


	<h3>On the table:</h3>
        <br>

        <table>
        <tr>
        <td width="50%">
            <a href="player.php?action=take"><div class="card back">*</div></a>
        </td>
        <td width="50%">
            <div id="zamin"><?php echo CardHTML($zamin, 1); ?></div>
        </td>
        </tr>
        </table>

        <br>

	<br><br><br>
	<h3>Your Hand:</h3>

        <ul class="hand">
            <?php

            for ($i = 1; $i <= (strlen($hand) / 2); $i++){
                $card = $hand[2 * $i - 2] . $hand[2 * $i - 1];
                echo CardHTML($card, 0);
            }

            ?>
        </ul>
</td>


<td width="30%" style="vertical-align:top">

        <h3>Game log:</h3>

            <br>
            <textarea id="log" rows="12" cols="40" style="font-size: 15px;"><?php echo substr($log, -2000); ?></textarea>



</tr>
<tr>

<td width="40%" style="vertical-align:top">
</td>
<td width="30%" style="vertical-align:top">

</td>
<td width="30%" style="vertical-align:top">
</td>


</tr>
</table>
</div>
</center>
<script src="jquery.min.js" type="text/javascript"></script>
<script>
var textarea = document.getElementById('log');
textarea.scrollTop = textarea.scrollHeight;

function loadData() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        server_response = JSON.parse(this.responseText);
        document.getElementById("zamin").innerHTML = server_response[0];
        document.getElementById("log").innerHTML = server_response[1];
        document.getElementById("userlist").innerHTML = server_response[2];

        log = server_response[1].split("\n");
        if ((log[log.length-1] == "___________ NEW GAME ___________") && (refresh == 0)){
            window.location = "player.php";
            refresh = 1;
        }else {
            refresh = 0;
        }

        var textarea = document.getElementById('log');
        textarea.scrollTop = textarea.scrollHeight;
    }
  };
  xhttp.open("GET", "update.php", true);
  xhttp.send();
}

setInterval(function(){
   loadData()
}, 1000);
</script>
</body>
</html>
