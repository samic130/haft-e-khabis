<?php
session_start();

/*
Haft-e-Khabis

By Samic.
(samic.org)

Created on March 20, 2020 
Updated on March 23, 2020 

*/

include "database.php";


$user = $_SESSION["user"];
$game_id = $_SESSION["game_id"];

if (isset($_GET['user_boot'])){
    $user_boot = $_GET['user_boot'];
}else{
    ShowText_Exit("No user was given!");
}


$sql = "SELECT * FROM `game` WHERE game='{$game_id}' ORDER BY ID";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
while ($rows = mysqli_fetch_array($result)) {
    if ($rows['user'] != "zamin"){
        $admin = $rows['user'];
        break;
    }
}
if ($admin != $user) {
    ShowText_Exit("You're not the admin!");
}


// Get the latest log
$sql = "SELECT `gamelog` FROM `log` WHERE game='{$game_id}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
$rows = mysqli_fetch_array($result);
$log = $rows['gamelog'];


$news = $user . " booted " . $user_boot . " at " . date("h:i:s");
$log .= "\n" . $news;
$sql = "UPDATE `log` SET gamelog='{$log}' WHERE game='{$game_id}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));


$sql = "DELETE FROM `game` WHERE game='{$game_id}' AND user='{$user_boot}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));


ShowText_Exit($user_boot . " is booted!<br>Close this window.");


?>
