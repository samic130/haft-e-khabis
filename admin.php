<?php
session_start();

/*
Haft-e-Khabis

By Samic.
(samic.org)

Created on March 20, 2020 
Updated on April 3, 2020 

*/

include "database.php";

function card_gen($num){
    $card_nums = array("A", "2", "3", "4", "5", "6", "7", "8", "9", "0", "J", "Q", "K");    // 0 is 10
    $card_suits = array("D", "C", "H", "S");    // diamonds (♦), clubs (♣), hearts (♥) and spades (♠)
    $cards = '';
    for ($i = 0; $i < $num; $i++){
        $cards = $cards . $card_nums[rand(0, 12)] . $card_suits[rand(0, 3)];
    }
return $cards;
}


$game_id = time() - 1000000000;
$game_id = base_convert($game_id, 10, 36);


if (isset($_GET['game_id'])){
    $game_id = mysqli_real_escape_string($DBlink, $_GET['game_id']);
}



if (isset($_GET['newdeal'])){

    // update everyones' hand
    //$sql = "UPDATE `game` SET hand='' WHERE game='{$game_id}'";
    //$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));

    $sql = "SELECT * from `game` WHERE game='{$game_id}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    while ($rows = mysqli_fetch_array($result)) {
        if ($rows['user'] != "zamin"){

            $sql2 = "UPDATE `game` SET hand='" . card_gen(7) . "' WHERE game='{$game_id}' AND user='{$rows['user']}'";
            $result2 = mysqli_query($DBlink, $sql2) or die(mysqli_error($DBlink));

        }else {

            $sql2 = "UPDATE `game` SET hand='" . card_gen(1) . "' WHERE game='{$game_id}' AND user='zamin'";
            $result2 = mysqli_query($DBlink, $sql2) or die(mysqli_error($DBlink));

        }
    }

    // Get the latest log
    $sql = "SELECT `gamelog` FROM `log` WHERE game='{$game_id}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    $rows = mysqli_fetch_array($result);
    $log = $rows['gamelog'];

    $log .=  "\n" . $_SESSION["user"] . " restarted the game.\n___________ NEW GAME ___________";
    $sql = "UPDATE `log` SET gamelog='{$log}' WHERE game='{$game_id}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));

    ShowText_Exit("A new round was started. Go back to the previous page to play.<br><br>You can close this window.");

}
else{

    $sql = "SELECT `user` FROM `game` WHERE game='{$game_id}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    if (mysqli_num_rows($result) > 0)  ShowText_Exit("Invalid Game ID<br>Select something else.");

    $sql = "INSERT INTO `game` (game, user, hand) VALUES ('{$game_id}', 'zamin', '" . card_gen(1) . "')";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));

    $sql = "INSERT INTO `log` (game, gamelog) VALUES ('{$game_id}', '')";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));

    ShowText_Exit("A new invitation link was created.<br><br>Give this link to the each player:<br><br><a href=https://haft.samic.org/?game_id={$game_id}>https://haft.samic.org/?game_id={$game_id}</a>");

}


?>

