<?php
session_start();

/*
Haft-e-Khabis

By Samic.
(samic.org)

Created on March 20, 2020 
Updated on March 23, 2020 

*/

include "database.php";

if (! isset($_SESSION["user"]))  ShowText_Exit("You're not logged in! Click on the invitation link again");

$user = $_SESSION["user"];
$game_id = $_SESSION["game_id"];


// Get the latest log
$sql = "SELECT `gamelog` FROM `log` WHERE game='{$game_id}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
$rows = mysqli_fetch_array($result);
$log = $rows['gamelog'];


if (isset($_REQUEST['chat'])){

    $msg = mysqli_real_escape_string($DBlink, $_REQUEST['chat']);

    $news = $user . ":  " . $msg;
    $log .= "\n" . $news;
    $sql = "UPDATE `log` SET gamelog='{$log}' WHERE game='{$game_id}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));

}



// ================================== END OF MAIN CODE ==================================

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Haft e Khabis</title>
    <link rel="stylesheet" type="text/css" href="cards.css" media="screen" />
    <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="cards-ie.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
        <link rel="stylesheet" type="text/css" href="cards-ie9.css" media="screen" />
    <![endif]-->
    <!-- the following js and css is not part of the CSS cards, but only for this example page -->
</head>
<body>
<center>

      <form action="chat.php" method="POST">
            Chat: <input type="text" id="chat" name="chat" size="30" style="font-size: 15px;" autocomplete="off">
      </form>

</center>
<script>document.getElementById('chat').focus();</script>
</body>
</html>
